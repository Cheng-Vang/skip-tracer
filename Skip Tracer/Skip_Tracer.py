# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------Libraries-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import re
import os
import skip
import json
import pandas
import tracer

from datetime import datetime

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Functions---------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

def skip_trace():
    """
    Commence skip tracing operation
    """

    dataframe, path = tracer.select()

    owners = tracer.procure_skips(dataframe)

    # Initialize package
    dlc = {}
    dlc['dataframe'] = dataframe
    
    search_state = re.search('[aA-zZ]+ [0-9]+-[0-9]+-[0-9]+', path).group(0)
    state = re.match('[aA-zZ]+', search_state).group(0)

    dlc['state'] = state

    total_skips = 0
    skip_counter = 1
    
    # Determine total skips
    for data in range(0,len(owners)):
        if owners[data] != {}:
            total_skips += 1

    print('There is/are ' + str(total_skips) + ' skip(s) for this set')

    driver = skip.login()

    start = datetime.now()     # Mark start time for log
    
    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)
    
    cities = config['cities']
        
    # Dataset session
    for data in range(0,len(owners)):
        
        if owners[data] == {}:
            pass
        
        else:
                                
            print('######## Analyzing skip ' + str(skip_counter) + ' out of ' + str(total_skips) + ' ########')

            # Reset vars per skip
            dlc['skip_found'] = False
            dlc['owners'] = []
            dlc['status_counter'] = 1
            total_targets = 0
            target_counter = 1
            target_number = 1

            # Fetch all respective owners for the current skip
            for index in range(0, len(owners[data])):
            
                try:
                    owners[data]['Owner' + str(index)]
                    
                except KeyError:
                    pass
                
                else:
                    dlc['owners'].append(owners[data]['Owner' + str(index)])
                    total_targets += 1

            print('Skip ' + str(skip_counter) + ' has the following ' + str(total_targets) + ' potential target(s) to skip:')

            # Gather potential targets
            for owner in owners[data]:

                if owner != 'Address':
                    print(str(target_number) + '. ' + owners[data][owner])
                    target_number += 1

            # Process each owner hash
            for owner in owners[data]:
                                
                # Skip Address hash
                if owner == 'Address':
                    pass

                else:
                    print('^^^ Performing reconnaissance on potential target ' + str(target_counter) + ' out of ' + str(total_targets) + ' ^^^')

                    owner = owners[data][owner]
                    split_owner = owner.split()

                    # Entity test
                    for term in config['entity']:

                        if term in split_owner:
                            process_or_pass = 'pass'
                            break

                        else:
                            process_or_pass = 'process'

                    # Process only owners that succeed in the entity test
                    if process_or_pass == 'process':

                        last_name = split_owner[0]
                        first_name = split_owner[1]

                        
                        # Check for mid initial
                        try:       
                            mid_name = split_owner[2]

                        except IndexError:
                            mid_initial = ''

                        else:
                            mid_initial = re.match('[aA-zZ]', mid_name).group(0)

                        address = owners[data]['Address']       # Fetch address

                        route = True        # Initialize route switch for city check

                        # Fetch city or mark incompatible city parameter
                        for city in cities:

                            if city in address:
                                city = city
                                route = True
                                break

                            else:
                                route = False

                        if route == True:
                            
                            # Initialize vars for current skip
                            dlc['first_name'] = first_name
                            dlc['mid_initial'] = mid_initial
                            dlc['last_name'] = last_name
                            dlc['address'] = address
                            dlc['city'] = city
                            dlc['data'] = data
                            dlc['top_priority_list'] = []
                            dlc['high_priority_list'] = []

                            print('Target ' + str(target_counter) + ' - ' + owner + ' cleared for skip')
                            
                            print('Attempting skip for target ' + str(target_counter) + ' - ' + owner)

                            dlc, driver = skip.initiate_skip_trace(dlc, driver)          # Initiate skip trace

                            if dlc['skip_found'] == False:
                                print('zzz Target ' + str(target_counter) + ' - ' + owner + ' was untraceable zzz')
                                dlc['dataframe'].loc[data, 'Skip Trace' + str(dlc['status_counter']) + ' Status'] = 'Untraceable'    # Mark skip as untraceable should skip unravel no leads
                                dlc['status_counter'] += 1
                            
                        else:
                            print('*** Bypassing target ' + str(target_counter) + ' - ' + owner + ' due to incompatible city parameter ***')
                            dlc['dataframe'].loc[data, 'Skip Trace' + str(dlc['status_counter']) + ' Status'] = 'Incompatible City Parameter'       # Mark inconclusive city parameter
                            dlc['status_counter'] += 1
                            
                    else:
                        print('*** Bypassing skip ' + str(target_counter) + ' - ' + owner + ' due to target being an entity ***')
                        dlc['dataframe'].loc[data, 'Skip Trace' + str(dlc['status_counter']) + ' Status'] = 'Entity'            # Mark entity
                        
                    target_counter += 1
                
            skip_counter += 1
             
    driver.quit()
                    
    dlc['dataframe'].to_excel(path)

    end = datetime.now()       # Mark end time for log
    
    date = datetime.today().strftime('%m-%d-%Y')

    tracer.log(start, end, date)

    return dlc


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    
    dlc = skip_trace()
