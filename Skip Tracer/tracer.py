# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------Libraries-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import os
import json
import time
import pandas
import tkinter
import subprocess
import tkinter.messagebox
import speech_recognition

from tkinter import Tk
from fnmatch import fnmatch
from pywinauto import mouse
from datetime import datetime
from pywinauto.keyboard import SendKeys
from selenium.webdriver.common.by import By
from tkinter.filedialog import askopenfilename
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Functions---------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


def select():
    """
    Initialize search arguments based on specified datum
    """

    file_check = False

    # Prompt for dataset
    while file_check == False:

        Tk().withdraw()
        tkinter.messagebox.showinfo("Dataset Selection", "Choose a dataset")

        path = askopenfilename()

        if path.lower().endswith('.xlsx'):  # Ensure file is a dataset
            datacheck = True

            if datacheck == True:
                
                try:
                    dataframe = pandas.read_excel(path, 'Sheet1')
                    dataframe.loc[0, 'Site Address']  # Minor check for dataset's validity
                    
                except:
                    print('Invalid selection. Please try again.')
                    
                else:
                    file_check = True
        else:
            print('Please select a dataset')

    return dataframe, path


def procure_skips(dataframe):
    """
    Procure skips
    """

    owners = []
    
    for data in range(dataframe.shape[0]):
        
        skip_counter = 1
        skip_column = True

        owner_list = {}
        
        # Process each owner per index
        while skip_column == True:

            # Check for addtional owners
            try:
                
                if pandas.isnull(dataframe.at[data,'Skip Trace'+str(skip_counter)]) == True:
                    process_or_pass = 'pass'

                else:
                    process_or_pass = 'process'
                    
            except:
                owner_column = False
                break

            else:
                
                # Add owner into current owner_list if nonexistent 
                if process_or_pass == 'process':

                    owner = dataframe.loc[data, 'Skip Trace' + str(skip_counter)]

                    if owner not in owner_list:
                        owner_list['Owner' + str(skip_counter)] = owner
                        owner_list['Address'] = dataframe.loc[data, 'Site Address']
                    
                else:
                    skip_column = False    
                    break
                  
            skip_counter += 1
            
        owners.append(owner_list)       # Update owners' list to reflect current index
            
    return owners


def solve_recaptcha(driver):
    """
    Solve Recaptcha V2
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    os.chdir(config['downloads'])
    
    print('~~~ Commencing download ~~~')

    time.sleep(1)    # Watch team backup for failed challenges

    mouse.click(button='left', coords=(790, 505))   # Download audio

    checker = False

    while checker == False:

        checker = download_complete(file_type = 'mp3')

    time.sleep(2)    # Watch team backup for checker

    mp3 = str(os.listdir()).strip('[\']')   # Retrieve audio's name 

    file_input = config['mule'] + mp3       # Retrieve full audio path
        
    wav = mp3.replace('.mp3','.wav')        # Set new audio as wav format

    file_output = config['root'] + '\\' + wav      # Set converted full audio path

    print('Converting audio to wav')

    subprocess.call(['ffmpeg', '-i', file_input, file_output])  # Convert audio to wav

    os.remove(mp3)

    os.chdir(config['root'])

    r = speech_recognition.Recognizer()     # Instantiate a Recognizer

    recap = speech_recognition.AudioFile('audio.wav')   # Send wav file into Recognizer's AudioFile method

    # Process wav
    with recap as source:
        audio = r.record(source)

    print('Conducting speech recognition')

    text = r.recognize_google(audio)    # Engage speech recognition

    os.remove(wav)

    print('Submitting results')
    mouse.click(button='left', coords=(790, 455))   # Click input box for audio challenge
    SendKeys(text)                                  # Input text from speech recognition
    mouse.click(button='left', coords=(875, 565))   # Verify

    print('Substantiating speech recognition')

    try:
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="people-tab"]')))
        
    except TimeoutException:
        pass_or_fail = 'fail'
        
    else:
        pass_or_fail = 'pass'

    return pass_or_fail, driver


def download_complete(file_type):
    """
    Download completion detection
    """

    checker = False

    if file_type == 'pdf':
        set_type = '*.pdf'
        
    else:
        set_type = '*.mp3'

        
    for file in os.listdir('.'):
        
        if fnmatch(file, set_type):
            print('~~~ Download Complete ~~~')
            checker = True
            
        else:
            checker = False
            
    return checker


def log(start, end, date):
    """
    Log evolution
    """
    
    print('---------------- Evolution Complete  -----------------')

    parent_dir = os.getcwd()
    
    log_path =  parent_dir + '\\logs\\' + date + '.txt'
    log = open(log_path, 'w')

    print('Started at ' + str(start))
    log.write('Started at ' + str(start))
    
    print('Ended at ' + str(end))
    log.write('\nEnded at ' + str(end))

    delta = end - start
    seconds = int(round(delta.total_seconds()))
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)

    print('Evolution duration: {:d} hour(s) and {:02d} minute(s) and {:02d} second(s)'.format(hours, minutes, seconds))
    log.write('\nEvolution duration {:d} hour(s) and {:02d} minute(s) and {:02d} second(s)'.format(hours, minutes, seconds))
    log.close()


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass
