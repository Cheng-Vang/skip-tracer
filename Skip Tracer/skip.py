# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------Libraries-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import re
import os
import time
import json
import tracer
import pandas

from shutil import move
from pywinauto import mouse
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Functions---------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


def login():
    """
    Login
    """
    
    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    profile = webdriver.FirefoxProfile()
    profile.set_preference('browser.download.folderList', 2)
    profile.set_preference('browser.download.manager.showWhenStarting', False)
    profile.set_preference('browser.download.dir', config['downloads'])
    profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'application/pdf,application/zip,audio/mpeg,audio/mpeg3,audio/x-mpeg-3,video/mpeg,video/x-mpeg,audio/mp3')
    profile.set_preference('browser.download.useDownloadDir', True)
    profile.set_preference('media.play-stand-alone', False)
    profile.set_preference('pdfjs.disabled', True)
    path = config['older_geckodriver_path']
    driver = webdriver.Firefox(executable_path=path, firefox_profile=profile)

    print('Loading site')
    driver.get(config['url'])

    driver.maximize_window()

    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,'//*[@id="login-email"]')))

    print('Entering credentials')
    email = driver.find_element_by_xpath('//*[@id="login-email"]')
    email.clear()
    email.send_keys(config['email'])

    password = driver.find_element_by_xpath('//*[@id="login-password"]')
    password.clear()
    password.send_keys(config['password'])

    driver.find_element_by_xpath('//*[@id="submit"]').click()

    print('Processing reCAPTCHA')
    time.sleep(1)

    mouse.click(button='left', coords=(670, 715))   # Proceed to audio challenge

    time.sleep(1)

    solving_recaptcha = True

    # Solve recaptcha
    while solving_recaptcha == True:

        pass_or_fail, driver = tracer.solve_recaptcha(driver)

        if pass_or_fail == 'pass':
            print('::: Solved reCAPTCHA :::')
            solving_recaptcha = False
            break

        else:
            print('::: Reattempting reCAPTCHA :::')
            mouse.click(button='left', coords=(680, 595))
    
    return driver


def initiate_skip_trace(dlc, driver):
    """
    Set seach parameters and engage skip trace
    """
    
    if dlc['mid_initial'] == '':
        print('Inputting ' + dlc['first_name'] + ' ' + dlc['last_name'])
        dlc['cur_skip'] = dlc['first_name'] + ' ' + dlc['last_name']
        
    else: 
        print('Inputting ' + dlc['first_name'] + ' ' + dlc['mid_initial'] + ' ' + dlc['last_name'])
        dlc['cur_skip'] = dlc['first_name'] + ' ' + dlc['mid_initial'] + ' ' + dlc['last_name']

    driver.execute_script('window.scrollTo(0, 0);')
    time.sleep(1)
    
    driver.find_element_by_xpath('//*[@id="people-tab"]').click()
    
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="fn1"]')))
    first_name = driver.find_element_by_xpath('//*[@id="fn1"]')
    first_name.clear()
    first_name.click()
    first_name.send_keys(dlc['first_name'])

    if dlc['mid_initial'] != '':

        mid_initial = driver.find_element_by_xpath('//*[@id="mn1"]')
        mid_initial.clear()
        mid_initial.click()
        mid_initial.send_keys(dlc['mid_initial'])
        
    last_name = driver.find_element_by_xpath('//*[@id="ln1"]')
    last_name.clear()
    last_name.click()
    last_name.send_keys(dlc['last_name'])

    print('Setting city to ' + dlc['city'])
    city = driver.find_element_by_xpath('//*[@id="city1"]')
    city.clear()
    city.click()
    city.send_keys(dlc['city'])

    print('Setting state to ' + dlc['state'])
    select_state = Select(driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[1]/div[2]/div/div/div/div[1]/div/form[1]/div[1]/div[5]/div/select'))
    select_state.select_by_visible_text(dlc['state'])

    print('Retrieving skips')
    driver.find_element_by_xpath('//*[@id="person-search-btn-lg"]').click()

    dlc, driver, search_results = fetch_search_results(dlc, driver)

    # Route based on search results
    if search_results == False:
        print('No skips found')
        dlc['skip_found'] = False    # Mark inconclusive skip
    
    else:
        dlc, driver = main_skip(dlc, driver)    # Proceed to skip mainframe

    return dlc, driver


def fetch_search_results(dlc, driver):
    """
    Retrieve search result(s)
    """

    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[1]/div/div/div/div[2]/h2/span[1]')))

    driver, converted_search_results = convert_search_results(driver)
    
    dlc['100+'] = False     # Reset 100+ switch every search
    
    if converted_search_results == '0':
        search_results  = False

    elif converted_search_results == '100+ reports':

        WebDriverWait(driver, 8).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[3]/div[1]/div/div/div[1]/div/a')))
        print('Dismissing notification')
        driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[3]/div[1]/div/div/div[1]/div/a').click()
        
        dlc['100+'] = True
        search_results = True

    else:
        search_results = True
    
    return dlc, driver, search_results


def convert_search_results(driver):
    """
    Convert seach result(s)
    """

    converted_search_results = str(driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[1]/div/div/div/div[2]/h2/span[1]').text)

    return driver, converted_search_results

    
def main_skip(dlc, driver):
    """
    Skip mainframe
    """

    print('Prepping lists')
    dlc, driver = prep_lists(dlc, driver)

    skip_switch = False
    dlc['search_list'] = 'top'  # Reset search list every skip
    dlc['skip_found'] = False   # Reset skip status every skip

    while skip_switch == False:
        
        # Check top skips
        if len(dlc['top_priority_list']) != 0:
            print('Engaging top priority skips')
            dlc, driver = skip_list(dlc, driver)

        else:
            dlc['search_list'] = 'high'

        # If found in top skips then terminate all skips
        if dlc['skip_found'] == True:
            skip_switch = True
            break

        # Check high skips
        if len(dlc['high_priority_list']) != 0:
            print('Engaging high priority skips')
            dlc, driver = skip_list(dlc, driver)

        else:
            dlc['search_list'] = 'standard'

        # If found in high skips then terminate all skips
        if dlc['skip_found'] == True:
            skip_switch = True
            break

        print('Engaging standard skips')
        # Engage standard skip as a last resort
        dlc, driver = standard_skip(dlc, driver)

        # Explicitly terminate after combing all skips
        skip_switch = True
        break
        
    # Process skip if found
    if dlc['skip_found'] == True:
        print('+++ Target ' + str(dlc['status_counter']) + ' - '+ dlc['cur_skip'] + ' identified +++')
        dlc, driver = process_skip(dlc, driver)

    return dlc, driver


def prep_lists(dlc, driver):
    """
    Prepare top and high priority lists
    """

    dlc['a_counter'] = 1    # Reset a counter every prep
    skip_counter = True

    while skip_counter == True:

        # Check if current name exist
        try:
            driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/a[' + 
                                         str(dlc['a_counter']) +']/div/div/div/div[1]/div[1]/div[2]/div[1]')
            
        except NoSuchElementException:
            skip_counter = False
            break
            
        else:
            dlc, driver = gen_lists(dlc, driver)
            dlc['a_counter'] += 1

    return dlc, driver

def extract_city(xpath_city):
    """
    Extract city
    """
    
    raw_city = str(xpath_city.text)
    raw_city_split = raw_city.split(',')
    city = re.sub('^ ', '', raw_city_split[0])

    return city

def extract_gen_relative(xpath_relative):
    """
    Extract relative
    """

    raw_relative = str(xpath_relative.text)
    relative = re.sub('^ ', '', raw_relative)

    return relative


def gen_lists(dlc, driver):
    """
    Generate top and high priority lists
    """

    city_list = []

    # Append first city (i.e. it has a different Xpath vs consecutive cities) and/or route an empty city list(i.e. no cities xpath = //*[@id="emberxxxx"])
    try:
        xpath_city = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/a[' +
                                     str(dlc['a_counter']) + ']/div/div/div/div[3]/p')

    except NoSuchElementException:
        proceed = False

    else:
        city = extract_city(xpath_city)
        city_list.append(city)
        proceed = True

    # If first city checks out then append further given cities
    if proceed == True:

        city_p_counter = 1
        city_switch = True

        while city_switch == True:

            try:
                xpath_city = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/a[' +
                                             str(dlc['a_counter']) + ']/div/div/div/div[3]/div/p[' + str(city_p_counter) + ']')

            except NoSuchElementException:
                city_switch = False
                break

            else:
                city = extract_city(xpath_city)
                city_list.append(city)
                city_p_counter += 1
    
    relative_list = []
    relative_p_counter = 1
    relative_switch = True
    
    # Append relatives or route an empty relative list
    while relative_switch == True:
        
        try:
            xpath_relative = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/a[' +
                                                          str(dlc['a_counter']) + ']/div/div/div/div[4]/div/p[' + str(relative_p_counter) + ']')

        except NoSuchElementException:
            relative_switch = False
            break

        else:
            relative = extract_gen_relative(xpath_relative)
            relative_list.append(relative)
            relative_p_counter += 1

    add_city = False
    add_relative = False

    # Check to see if city is in city list
    if dlc['city'] in city_list:
        add_city = True

    # Check to see if any owners are in the relative list
    for relative in relative_list:

        check_out_switch = False    # Reset check out switch every iteration

        # Iterate every owner per relative check
        for owner in dlc['owners']:
            
            key_words = owner.split(' ')

            checks_out = False     # Reset check out switch every iteration

            # Format owner name and check each word in owner name for current relative
            for word in key_words:

                check_word = word.capitalize()

                # Word found so mark as a possible match 
                if check_word in relative:
                    checks_out = True

                # Immediately break as owner is not current relative and proceed with next owner
                else:
                    checks_out = False
                    break

            # All words match current relative so activate switch to terminate
            if checks_out == True:
                check_out_switch = True
                break

        # If check out switch is activated then immediately break and switch relative flag
        if check_out_switch == True:
            add_relative = True
            break
    
    # Append results as necessary
    if add_city == True and add_relative == True:
        dlc['top_priority_list'].append(dlc['a_counter'])
        
    elif add_city == True and add_relative == False:
        dlc['high_priority_list'].append(dlc['a_counter'])
        
    elif add_city == False and add_relative == True:
        dlc['high_priority_list'].append(dlc['a_counter'])
        
    return dlc, driver


def skip_list(dlc, driver):
    """
    Skip top or high list
    """

    # Set which list to search
    if dlc['search_list'] == 'top':
        search_list = dlc['top_priority_list']
        
    else:
        search_list = dlc['high_priority_list']

    # Search list
    for a_counter in search_list:

        dlc['a_counter'] = a_counter
        dlc, driver = skip_repo(dlc, driver)

        # Immediately terminate search if target identified
        if dlc['skip_found'] == True:
            break
                
    # Activate high skips in the event top skips are inconclusive
    if dlc['skip_found'] == False and dlc['search_list'] == 'top':  
        dlc['search_list'] = 'high'

    # Activate standard skips in the event high skips are inconclusive
    elif dlc['skip_found'] == False and dlc['search_list'] == 'high':
        dlc['search_list'] = 'standard'

    return dlc, driver


def standard_skip(dlc, driver):
    """
    Standard skip
    """
    
    dlc['a_counter'] = 1
    skip_counter = True

    while skip_counter == True:

        # Bypass checked top and high skips
        if dlc['a_counter'] in dlc['top_priority_list'] or dlc['a_counter'] in dlc['high_priority_list']:
            pass

        else:
            
            # Check if current name exist
            try:
                raw_name = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/a[' + 
                                             str(dlc['a_counter']) +']/div/div/div/div[1]/div[1]/div[2]/div[1]')
                
            except NoSuchElementException:
                skip_counter = False
                break
                
            else:
                dlc, driver = skip_repo(dlc, driver)
            
            # Immediately terminate skips if target identified
            if dlc['skip_found'] == True:
                skip_counter = False
                break

        dlc['a_counter'] += 1

    return dlc, driver


def extract_name(raw_name):
    """
    Extract name
    """

    name = raw_name.text

    return name


def extract_location(raw_location):
    """
    Extract location
    """

    location = raw_location.text

    return location


def extract_vitals(raw_cur_addy_or_deceased_text):
    """
    Extract vitals
    """

    cur_addy_or_deceased_text = raw_cur_addy_or_deceased_text.text

    return cur_addy_or_deceased_text


def extract_address(fetch_address):
    """
    Extract address
    """

    skip_address = fetch_address.text

    return skip_address


def extract_repo_relative(fetch_relative):
    """
    Extract relative
    """

    skip_relative = fetch_relative.text

    return skip_relative


def check_addresses(dlc, driver):
    """
    Check report's addresses
    """

    address_switch = False  # Reset address switch every iteration
    address_counter = 2     # Reset address counter every iteration
    address_found = False   # Reset address found every iteration

    # Check given addresses to see if any match
    while address_switch == False:
        
        try:
            fetch_address = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[3]/div[16]/div[1]/div/div[2]/' + 
                                                         'div[3]/div[3]/div[1]/div[' + str(address_counter) +']/div[2]/div/div[2]/h4')

        except NoSuchElementException:
            address_switch = True
            break

        else:
            skip_address = extract_address(fetch_address)
            address_split = dlc['address'].split(' ')

            for word in address_split:
    
                if word.lower() in skip_address.lower():    # Format uniformity for comparison
                    address_found = True    # Mark address found if word matches

                else:                       # Othwerise immediately break if word is nonexistent and reset address found as False in the event preceding words matched
                    address_found = False
                    address_switch = True
                    break

            # If address matches then activate switch to process NOTE: Driver should still be in report
            if address_found == True:  
                dlc['skip_found'] = True
                return dlc, driver
                
            address_counter += 1

    return dlc, driver


def check_relatives(dlc, driver):
    """
    Check report's relatives
    """

    relative_switch = False  # Reset relative switch every iteration
    relative_counter = 1     # Reset relative counter every iteration
    relative_found = False   # Reset relative found every iteration

    # Check given relatives to see if any match
    while relative_switch == False:
        
        try:
            fetch_relative = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[3]/div[16]/div[1]/div/div[2]/' +
                                                          'div[5]/div[1]/div[' + str(relative_counter) + ']/div[1]/div[1]/div[2]/h4')

        except NoSuchElementException:
            relative_switch = True
            break

        else:
            skip_relative = extract_repo_relative(fetch_relative)
            check_out_switch = False    # Reset check switch every skip

            # Iterate every owner per relative check
            for owner in dlc['owners']:
                
                key_words = owner.split(' ')
                checks_out = False      # Reset owner counter every iteration

                # Format owner name and check each word in owner name for current relative
                for word in key_words:  
                    check_word = word.capitalize()

                    # Word found so mark as a possible match 
                    if check_word in skip_relative:
                        checks_out = True

                    # Immediately break as owner is not current relative and reset checks out in the event prior words matched
                    else:
                        checks_out = False
                        break

                # # If relative matches then activate switch to process NOTE: Driver should still be in report
                if checks_out == True:
                    dlc['skip_found'] = True
                    return dlc, driver

                relative_counter += 1

    return dlc, driver


def skip_repo(dlc, driver):
    """
    Skip given report
    """
        
    # Proceed into report
    raw_name = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/a[' + 
                                  str(dlc['a_counter']) +']/div/div/div/div[1]/div[1]/div[2]/div[1]')
    
    name = extract_name(raw_name)
    dlc['name_skip'] = name
    
    raw_location = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/a[' +
                                        str(dlc['a_counter']) + ']/div/div/div/div[1]/div[1]/div[2]/div[2]')
    
    location = extract_location(raw_location)
    dlc['location_skip'] = location
    
    print('??? Investigating ' + dlc['name_skip'] + ' in ' + dlc['location_skip'] + ' ???')  
    
    driver.execute_script("arguments[0].scrollIntoView();", raw_name)
    time.sleep(1)
    driver.execute_script("arguments[0].click();", raw_name)

    # Wait for "Current Address" or "Deceased Text" if deceased
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
        '/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[3]/div[16]/div[1]/div/div[2]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/strong')))

    dlc['deceased'] = False # Reset deceased flag every iteration
    
    #Check if deceased
    raw_cur_addy_or_deceased_text = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[3]/div[16]/div[1]/div/div[2]/' +
                                                             'div[1]/div/div[1]/div[2]/div[2]/div[2]/div[1]/strong')

    cur_addy_or_deceased_text = extract_vitals(raw_cur_addy_or_deceased_text)
    
    # If deceased then flag skip
    if cur_addy_or_deceased_text == 'Deceased':
        dlc['deceased'] = True

    # Check addresses
    dlc, driver = check_addresses(dlc, driver)

    # Report if any match
    if dlc['skip_found'] == True:
        return dlc, driver

    # Check relatives
    dlc, driver = check_relatives(dlc, driver)

    # Report if any match
    if dlc['skip_found'] == True:
        return dlc, driver
    
    # If the report is inconclusive then revert driver back to search results
    print('--- False positive on ' + dlc['name_skip'] + ' in ' + dlc['location_skip'] + ' ---')
    print('Rengaging skips')
    driver.execute_script("history.back();")

    # If applicable, dismiss 100+ notification before proceeding
    if dlc['100+'] == True:
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[3]/div[1]/div/div/div[1]/div/a')))
        print('Dismissing notification')
        driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[3]/div[1]/div/div/div[1]/div/a').click()

    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[1]/div/div/div/div[2]/h2/span[1]')))

    return dlc, driver


def extract_deceased_stats(deceased):
    """
    Extract deceased stats
    """

    deceased_stats = deceased.text

    return deceased_stats


def extract_phone_numbers(dlc, driver):
    """
    Extract phone numbers
    """

    phone_switch = False
    phone_counter = 1

    while phone_switch == False:

        try:
            fetch_phone_stats = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[3]/' +
                                                              'div[16]/div[1]/div/div[2]/div[3]/div[1]/div[1]/div[' + str(phone_counter) + ']/div[2]')
        except NoSuchElementException:       
            phone_switch = True
            break
            
        else:   
            phone_stats = fetch_phone_stats.text

            if 'Not' in phone_stats:
                pass
            
            else:
                dlc['dataframe'].loc[dlc['data'], 'Owner' + str(dlc['status_counter']) + ' Phone' + str(phone_counter)] = phone_stats
                
            phone_counter += 1

    return dlc, driver


def extract_emails(dlc, driver):
    """
    Extract emails
    """

    email_switch = False
    email_counter = 1

    while email_switch == False:

        try:
            fetch_email_stats = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[3]/' +
                                                              'div[16]/div[1]/div/div[2]/div[3]/div[2]/div[1]/div[' + str(email_counter) + ']/div[2]')
        except NoSuchElementException:       
            email_switch = True
            break
            
        else:   
            email_stats = fetch_email_stats.text
            dlc['dataframe'].loc[dlc['data'], 'Owner' + str(dlc['status_counter']) + ' Email' + str(email_counter)] = email_stats
            email_counter += 1

    return dlc, driver


def post_ops(dlc, config):
    """
    Perform post operations
    """
    
    # Fetch generated report name
    hold_path = str(os.listdir())
    mule_hold_1 = hold_path.strip('[')
    mule_hold_2 = mule_hold_1.strip('\'')
    mule_hold_3 = mule_hold_2.strip(']')
    pdf = mule_hold_3.strip('\'')

    os.chdir(config['profiles'])

    # Transfer report to profiles and relabel it
    mule = config['mule'] + pdf
    xfer = config['xfer'] + pdf
    move(mule, xfer)

    os.rename(pdf, dlc['name_skip'] + '.pdf')

    # Hyperlink report
    report = config['xfer'] + dlc['name_skip'] + '.pdf'
    dlc['dataframe'].loc[dlc['data'], 'Report' + str(dlc['status_counter'])] = '=HYPERLINK("{}","{}")'.format(report, dlc['name_skip'])

    os.chdir(config['root'])

    return driver


def process_skip(dlc, driver):
    """
    Process skip and download profile
    """

    # Vital check
    if dlc['deceased'] == True:
        
        print(dlc['name_skip'] + ' is deceased')
        deceased = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[3]/' +
                                                'div[16]/div[1]/div/div[2]/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[2]')

        deceased_stats = extract_deceased_stats(deceased)
        dlc['dataframe'].loc[dlc['data'], 'Skip Trace' + str(dlc['status_counter']) + ' Status'] = deceased_stats
        dlc['status_counter'] += 1
        
        return dlc, driver
  
    # Retrieve phone numbers
    print('Retrieving associated phone numbers')
    dlc, driver = extract_phone_numbers(dlc, driver)

    # Retrieve emails
    print('Retrieving associated emails')
    dlc, driver = extract_emails(dlc, driver)
            
    print('Downloading report')

    # Refer to config JSON for determination
    with open('../misc/config.json') as config:
        config = json.load(config)

    os.chdir(config['downloads'])
    
    download = driver.find_element_by_xpath('/html/body/div[2]/div/div[1]/div[2]/div[2]/div/div[3]/div[16]/div[1]/div/div[1]/div/nav/div[2]/div/div/div[1]/button')
    
    driver.execute_script("arguments[0].scrollIntoView();", download)
    time.sleep(1)
    print('~~~ Commencing download ~~~')
    driver.execute_script("arguments[0].click();", download)

    checker = False

    while checker == False:

        checker = tracer.download_complete(file_type = 'pdf')

    driver.switch_to.window(driver.window_handles[-1])
    driver.close()

    driver.switch_to.window(driver.window_handles[0])

    print('Perform post ops')
    dlc = post_ops(dlc, config)

    print('*** Target ' + str(dlc['status_counter']) + ' - ' + dlc['name_skip'] + ' processed ***')
    dlc['dataframe'].loc[dlc['data'], 'Skip Trace' + str(dlc['status_counter']) + ' Status'] = 'Traced'
    dlc['status_counter'] += 1

    return dlc, driver


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass
