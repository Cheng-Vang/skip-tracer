# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------Libraries/Imports-------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import os
import re
import pytest

import sys
sys.path.append(r'..\Skip Tracer')

import skip
import tracer

from unittest.mock import patch, MagicMock, Mock
from selenium.common.exceptions import NoSuchElementException

# ----------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------Test Cases/Test Suite----------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


@pytest.mark.parametrize('attempt', [
    ('First attempt'),
    ('Second+ attempt')
])
def test_login(fixture, attempt):
    """
    Ensure login based on successful reCAPTCHA attempt
    """

    if attempt == 'First attempt':
        first_attempt(fixture)

    else:
        second_plus_attempt(fixture)


def first_attempt(fixture):
    """
    Login after first reCAPTHCA attempt
    """

    dlc, driver = fixture

    with patch('skip.webdriver', autospec=True) as mock_webdriver, \
         patch('skip.WebDriverWait'), \
         patch('pywinauto.mouse.click'), \
         patch('skip.time'), \
         patch('tracer.solve_recaptcha', return_value = ('pass',driver)) as solve_recaptcha:

        driver = skip.login()

        assert solve_recaptcha.call_count == 1


def second_plus_attempt(fixture):
    """
    Login after second+ reCAPTCHA attempt
    """

    dlc, driver = fixture
    
    with patch('skip.webdriver', autospec=True) as mock_webdriver, \
         patch('skip.WebDriverWait'), \
         patch('pywinauto.mouse.click'), \
         patch('skip.time'), \
         patch('tracer.solve_recaptcha', side_effect = [('fail',driver), ('pass',driver)]) as solve_recaptcha:

        driver = skip.login()

        assert solve_recaptcha.call_count == 2


@pytest.mark.parametrize('sitrep', [
    ('Identified skips'),
    ('No skips')
])
def test_initiate_skip_trace(fixture, sitrep):
    """
    Ensure routing based on fetched search results
    """

    if sitrep == 'Identified skips':
        identified_skips(fixture)
        
    else:
        no_skips(fixture)


def identified_skips(fixture):
    """
    Identified skips and proceed
    """

    dlc, driver = fixture

    with patch('skip.time'), \
         patch('skip.WebDriverWait'), \
         patch('skip.Select'), \
         patch('skip.fetch_search_results', return_value = (dlc, driver, True)), \
         patch('skip.main_skip', return_value = (dlc, driver)) as main_skip:

        dlc, driver = skip.initiate_skip_trace(dlc, driver)

        assert main_skip.call_count == 1
         

def no_skips(fixture):
    """
    No skips found so report back
    """

    dlc, driver = fixture

    with patch('skip.time'), \
         patch('skip.WebDriverWait'), \
         patch('skip.Select'), \
         patch('skip.fetch_search_results', return_value = (dlc, driver, False)):

        dlc, driver = skip.initiate_skip_trace(dlc, driver)

        assert dlc['skip_found'] == False


@pytest.mark.parametrize('search_results', [
    ('Standard'),
    ('100+'),
    ('None')
])
def test_fetch_search_results(fixture, search_results):
    """
    Ensure proper switches based on search results
    """

    if search_results == 'Standard':
        search_results_standard(fixture)

    elif search_results == '100+':
        search_results_100(fixture)

    else:
        search_results_none(fixture)

    
def search_results_standard(fixture):
    """
    Switches for standard search results
    """

    dlc, driver = fixture
    
    with patch('skip.WebDriverWait'), \
         patch('skip.convert_search_results', return_value = [driver, '10']):

        dlc, driver, search_results = skip.fetch_search_results(dlc, driver)

    assert search_results == True
    assert dlc['100+'] == False


def search_results_100(fixture):
    """
    Switches for 100+ search results
    """

    dlc, driver = fixture
    
    with patch('skip.WebDriverWait'), \
         patch('skip.convert_search_results', return_value = [driver, '100+ reports']):

        dlc, driver, search_results = skip.fetch_search_results(dlc, driver)

    assert search_results == True
    assert dlc['100+'] == True

    
def search_results_none(fixture):
    """
    Switches for no search results
    """

    dlc, driver = fixture
    
    with patch('skip.WebDriverWait'), \
         patch('skip.convert_search_results', return_value = [driver, '0']):

        dlc, driver, search_results = skip.fetch_search_results(dlc, driver)

    assert search_results == False
    assert dlc['100+'] == False


@pytest.mark.parametrize('sitrep', [
    ('Traced in top'),
    ('In top but traced in high'),
    ('Traced in high'),
    ('In high but traced in standard'),
    ('Traced in standard'),
    ('Untraceable')
])
def test_main_skip(fixture, skip_found_fixture, sitrep):
    """
    Ensure proper routing and switches of skip cases in main skip
    """

    if sitrep == 'Traced in top':
        sitrep_alpha(fixture, skip_found_fixture)

    elif sitrep == 'In top but traced in high':
        sitrep_bravo(fixture, skip_found_fixture)

    elif sitrep == 'Traced in high':
        sitrep_charlie(fixture, skip_found_fixture)

    elif sitrep == 'In high but traced in standard':
        sitrep_delta(fixture, skip_found_fixture)

    elif sitrep == 'Traced in standard':
        sitrep_echo(fixture, skip_found_fixture)

    else:
        sitrep_foxtrot(fixture)

        
def sitrep_alpha(fixture, skip_found_fixture):
    """
    Target traced in top
    """

    dlc, driver = fixture
    
    skip_found_dlc, driver = skip_found_fixture
    
    dlc['top_priority_list'].append(1)      # Inject an index into top priority
    
    with patch('skip.prep_lists', return_value = [dlc, driver]), \
         patch('skip.skip_list', return_value = [skip_found_dlc, driver]), \
         patch('skip.process_skip', return_value = [skip_found_dlc, driver]):

        dlc, driver = skip.main_skip(dlc, driver)

    assert dlc['skip_found'] == True


def sitrep_bravo(fixture, skip_found_fixture):
    """
    Target in top but traced in high
    """

    dlc, driver = fixture
    
    skip_found_dlc, driver = skip_found_fixture

    dlc['top_priority_list'].append(1)      # Inject an index into top priority
    dlc['high_priority_list'].append(1)     # Inject an index into high priority
    
    with patch('skip.prep_lists', return_value = [dlc, driver]), \
         patch('skip.skip_list', side_effect = [(dlc, driver),(skip_found_dlc, driver)]), \
         patch('skip.process_skip', return_value = [skip_found_dlc, driver]):

        dlc, driver = skip.main_skip(dlc, driver)

    assert dlc['skip_found'] == True


def sitrep_charlie(fixture, skip_found_fixture):
    """
    Target traced in high
    """

    dlc, driver = fixture
    
    skip_found_dlc, driver = skip_found_fixture
    
    dlc['high_priority_list'].append(1)      # Inject an index into high priority
    
    with patch('skip.prep_lists', return_value = [dlc, driver]), \
         patch('skip.skip_list', return_value = [skip_found_dlc, driver]), \
         patch('skip.process_skip', return_value = [skip_found_dlc, driver]):

        dlc, driver = skip.main_skip(dlc, driver)

    assert dlc['skip_found'] == True


def sitrep_delta(fixture, skip_found_fixture):
    """
    Target in high but traced in standard
    """

    dlc, driver = fixture
    
    skip_found_dlc, driver = skip_found_fixture

    dlc['high_priority_list'].append(1)     # Inject an index into high priority
    
    with patch('skip.prep_lists', return_value = [dlc, driver]), \
         patch('skip.skip_list', return_value = [dlc, driver]), \
         patch('skip.standard_skip', return_value = [skip_found_dlc, driver]), \
         patch('skip.process_skip', return_value = [skip_found_dlc, driver]):

        dlc, driver = skip.main_skip(dlc, driver)

    assert dlc['skip_found'] == True


def sitrep_echo(fixture, skip_found_fixture):
    """
    Target traced in standard
    """
    
    dlc, driver = fixture
    
    skip_found_dlc, driver = skip_found_fixture
    
    with patch('skip.prep_lists', return_value = [dlc, driver]), \
         patch('skip.standard_skip', return_value = [skip_found_dlc, driver]), \
         patch('skip.process_skip', return_value = [skip_found_dlc, driver]):

        dlc, driver = skip.main_skip(dlc, driver)

    assert dlc['skip_found'] == True
         

def sitrep_foxtrot(fixture):
    """
    Target untraceable
    """

    dlc, driver = fixture
    
    with patch('skip.prep_lists', return_value = [dlc, driver]), \
         patch('skip.skip_list', return_value = [dlc, driver]), \
         patch('skip.standard_skip', return_value = [dlc, driver]):

        dlc, driver = skip.main_skip(dlc, driver)

    assert dlc['skip_found'] == False


def test_prep_lists(fixture):
    """
    Ensure the proper iteration of list preparation and invocation of list generation
    """

    dlc, driver = fixture

    driver.find_element_by_xpath.side_effect = [True,True,NoSuchElementException]

    with patch('skip.gen_lists', return_value = [dlc, driver]) as gen_lists:

        dlc, driver = skip.prep_lists(dlc, driver)

    assert gen_lists.call_count == 2
    assert dlc['a_counter'] == 3


@pytest.mark.parametrize('generate', [
    ('Generate top'),
    ('Generate high from city'),
    ('Generate high from relative'),
    ('Standard')
])
def test_gen_lists(fixture, generate):
    """
    Ensure top and high lists are being generated properly
    """

    if generate == 'Generate top':
        gen_top(fixture)

    elif generate == 'Generate high from city':
        gen_high_1(fixture)

    elif generate == 'Generate high from relative':
        gen_high_2(fixture)

    else:
        gen_standard(fixture)
        

def gen_top(fixture):
    """
    Generate top list
    """
    
    dlc, driver = fixture
    driver.find_element_by_xpath.side_effect = [True, NoSuchElementException, True, NoSuchElementException]

    with patch('skip.extract_city', return_value = 'Spring Hill'), \
         patch('skip.extract_gen_relative', return_value = 'Wang Anita'):
        
        dlc, driver = skip.gen_lists(dlc, driver)

        assert dlc['top_priority_list'] == [1]


def gen_high_1(fixture):
    """
    Generate high list from city
    """
    
    dlc, driver = fixture
    driver.find_element_by_xpath.side_effect = [True, NoSuchElementException, True, NoSuchElementException]

    with patch('skip.extract_city', return_value = 'Spring Hill'), \
         patch('skip.extract_gen_relative', return_value = ''):
        
        dlc, driver = skip.gen_lists(dlc, driver)

        assert dlc['high_priority_list'] == [1]

    
def gen_high_2(fixture):
    """
    Generate high list from relative
    """
    
    dlc, driver = fixture
    driver.find_element_by_xpath.side_effect = [True, NoSuchElementException, True, NoSuchElementException]

    with patch('skip.extract_city', return_value = ''), \
         patch('skip.extract_gen_relative', return_value = 'Wang Anita'):
        
        dlc, driver = skip.gen_lists(dlc, driver)

        assert dlc['high_priority_list'] == [1]

    
def gen_standard(fixture):
    """
    Standard skip
    """
    
    dlc, driver = fixture
    driver.find_element_by_xpath.side_effect = [True, NoSuchElementException, True, NoSuchElementException]

    with patch('skip.extract_city', return_value = ''), \
         patch('skip.extract_gen_relative', return_value = ''):
        
        dlc, driver = skip.gen_lists(dlc, driver)

        assert dlc['top_priority_list'] == []
        assert dlc['high_priority_list'] == []

@pytest.mark.parametrize('sitrep', [
    ('Target ID\'ed in top'),
    ('Skips in top but negative ID'),
    ('Target ID\'ed in high'),
    ('Skips in high but negative ID')
])
def test_skip_list(fixture, skip_found_fixture, sitrep):
    """
    Ensure lists are properly skipped and switches are activated based on skip results
    """

    if sitrep == 'Target ID\'ed in top':
        sitrep_golf(skip_found_fixture)

    elif sitrep == 'Skips in top but negative ID':
        sitrep_hotel(fixture)

    elif sitrep == 'Target ID\ed in high':
        sitrep_india(skip_found_fixture)

    elif sitrep == 'Skips in high but negative ID':
        sitrep_juliet(fixture)


def sitrep_golf(skip_found_fixture):
    """
    Target identified in top list
    """

    dlc, driver = skip_found_fixture
    
    with patch('skip.skip_repo', return_value = [dlc, driver]):

        dlc, driver = skip.skip_list(dlc, driver)

    assert dlc['skip_found'] == True


def sitrep_hotel(fixture):
    """
    Skips in top but negative ID
    """

    dlc, driver = fixture
    
    with patch('skip.skip_repo', return_value = [dlc, driver]):

        dlc, driver = skip.skip_list(dlc, driver)

    assert dlc['skip_found'] == False
    assert dlc['search_list'] == 'high'


def sitrep_india(skip_found_fixture):
    """
    Target identified in high list
    """

    dlc, driver = skip_found_fixture

    # Mod stub for this case
    dlc['top_priority_list'] = []
    dlc['high_priority_list'] = [1]
    
    with patch('skip.skip_repo', return_value = [dlc, driver]):

        dlc, driver = skip.skip_list(dlc, driver)

    assert dlc['skip_found'] == True


def sitrep_juliet(fixture):
    """
    Skips in high but negative ID
    """

    dlc, driver = fixture

    # Mod stub for this case
    dlc['search_list'] = 'high'
    dlc['high_priority_list'] = [1]
    
    with patch('skip.skip_repo', return_value = [dlc, driver]):

        dlc, driver = skip.skip_list(dlc, driver)

    assert dlc['skip_found'] == False
    assert dlc['search_list'] == 'standard'


@pytest.mark.parametrize('sitrep', [
    ('Target ID\'ed in standard'),
    ('Skips in standard but negative ID')
])
def test_standard_skip(fixture, skip_found_fixture, sitrep):
    """
    Ensure standard skips are properly skipped and switches are activated based on skip results
    """

    if sitrep == 'Target ID\'ed in standard':
        sitrep_kilo(skip_found_fixture)

    else:
        sitrep_lima(fixture)


def sitrep_kilo(skip_found_fixture):
    """
    Target identified in standard
    """
        
    dlc, driver = skip_found_fixture
    
    driver.find_element_by_xpath.side_effect = [True, NoSuchElementException]

    with patch('skip.skip_repo', return_value = [dlc, driver]):

        dlc, driver = skip.standard_skip(dlc, driver)

    assert dlc['skip_found'] == True
    assert dlc['a_counter'] == 2


def sitrep_lima(fixture):
    """
    Skips in standard but negative ID
    """
        
    dlc, driver = fixture
    
    driver.find_element_by_xpath.side_effect = [True, NoSuchElementException]

    with patch('skip.skip_repo', return_value = [dlc, driver]):

        dlc, driver = skip.standard_skip(dlc, driver)

    assert dlc['skip_found'] == False
    assert dlc['a_counter'] == 2


@pytest.mark.parametrize('repo_status', [
    ('Address match'),
    ('Relative match'),
    ('Deceased'),
    ('False positive'),
    ('False positive and 100+')
])
def test_skip_repo(fixture, skip_found_fixture, repo_status):
    """
    Ensure skipped reports are properly routed and switches are activated based on skip results
    """

    if repo_status == 'Address match':
        address_match(skip_found_fixture)

    elif repo_status == 'Relative match':
        relative_match(fixture, skip_found_fixture)

    elif repo_status == 'Deceased':
        deceased_repo(skip_found_fixture)

    elif repo_status == 'False positive':
        false_positive(fixture, repo_status)

    else:
        false_positive(fixture, repo_status)


def address_match(skip_found_fixture):
    """
    Report's address matches target's address
    """

    dlc, driver = skip_found_fixture
    
    with patch('skip.extract_name', return_value = 'Kam K Sourivongs'), \
         patch('skip.extract_location', return_value = 'Tampa, FL'), \
         patch('skip.time'), \
         patch('skip.WebDriverWait'), \
         patch('skip.extract_vitals', return_value = 'Current Address'), \
         patch('skip.check_addresses', return_value = [dlc, driver]):
        
        dlc, driver = skip.skip_repo(dlc, driver)
         
        assert dlc['skip_found'] == True
         
         
def relative_match(fixture, skip_found_fixture):
    """
    Report's relative matches target's relative
    """

    dlc, driver = fixture
    skip_dlc, driver = skip_found_fixture
       
    with patch('skip.extract_name', return_value = 'Kam K Sourivongs'), \
         patch('skip.extract_location', return_value = 'Tampa, FL'), \
         patch('skip.time'), \
         patch('skip.WebDriverWait'), \
         patch('skip.extract_vitals', return_value = 'Current Address'), \
         patch('skip.check_addresses', return_value = [dlc, driver]), \
         patch('skip.check_relatives', return_value = [skip_dlc, driver]):
        
        dlc, driver = skip.skip_repo(dlc, driver)
         
        assert dlc['skip_found'] == True


def deceased_repo(skip_found_fixture):
    """
    Deceased reported
    """

    dlc, driver = skip_found_fixture
    
    with patch('skip.extract_name', return_value = 'Kam K Sourivongs'), \
         patch('skip.extract_location', return_value = 'Tampa, FL'), \
         patch('skip.time'), \
         patch('skip.WebDriverWait'), \
         patch('skip.extract_vitals', return_value = 'Deceased'), \
         patch('skip.check_addresses', return_value = [dlc, driver]):
        
        dlc, driver = skip.skip_repo(dlc, driver)
         
        assert dlc['deceased'] == True

    
def false_positive(fixture, repo_status):
    """
    False positive skip and 100+
    """

    dlc, driver = fixture

    # Mod stub for this case
    if repo_status == 'False positive and 100+':
        dlc['100+'] = True    
       
    with patch('skip.extract_name', return_value = 'Kam K Sourivongs'), \
         patch('skip.extract_location', return_value = 'Tampa, FL'), \
         patch('skip.time'), \
         patch('skip.WebDriverWait'), \
         patch('skip.extract_vitals', return_value = 'Current Address'), \
         patch('skip.check_addresses', return_value = [dlc, driver]), \
         patch('skip.check_relatives', return_value = [dlc, driver]):
        
        dlc, driver = skip.skip_repo(dlc, driver)
         
        assert dlc['skip_found'] == False

@pytest.mark.parametrize('vitals', [
    ('Alive'),
    ('Deceased')
])
def test_process_skip(skip_found_fixture, vitals):
    """
    Ensure proper processing of target based on vitals
    """

    if vitals == 'Alive':
        process(skip_found_fixture)
    else:
        process_deceased(skip_found_fixture)


def process(skip_found_fixture):
    """
    Process target as is
    """

    dlc, driver = skip_found_fixture

    with patch('skip.extract_phone_numbers', return_value = [dlc, driver]), \
         patch('skip.extract_emails', return_value = [dlc, driver]), \
         patch('skip.time'), \
         patch('tracer.download_complete', return_value = True), \
         patch('skip.post_ops', return_value = dlc) as post_ops:

        dlc, driver = skip.process_skip(dlc, driver)
        
    assert post_ops.call_count == 1
    assert dlc['dataframe'].loc[dlc['data'], 'Skip Trace1 Status'] == 'Traced'
    assert dlc['status_counter'] == 2
    

def process_deceased(skip_found_fixture):
    """
    Process target if deceased
    """

    dlc, driver = skip_found_fixture

    dlc['deceased'] = True      # Mod stub for this case

    with patch('skip.extract_deceased_stats', return_value = 'Deceased') as extract_deceased_stats:

        dlc, driver = skip.process_skip(dlc, driver)

    assert extract_deceased_stats.call_count == 1
    assert dlc['dataframe'].loc[dlc['data'], 'Skip Trace1 Status'] == 'Deceased'
    assert dlc['status_counter'] == 2
    
          
# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass
