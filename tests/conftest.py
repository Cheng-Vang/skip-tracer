# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------Libraries/Imports-------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import pytest
import pandas

from unittest.mock import MagicMock

from selenium import webdriver

# ----------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------Fixtures-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


@pytest.fixture(scope='function')
def dataframe_fixture():
    """
    Stub dataframe
    """

    dataframe = pandas.DataFrame({
                                  'Site Address':['','1234 Smith St No Name, CO','','',''],
                                  'Owner1':['','SMITH JOHN','','',''],
                                  'Owner2':['','SMITH JANE','','',''],
                                  'Skip Trace1':['','SMITH JOHN','','',''],
                                  'Skip Trace2':['','SMITH JANE','','','']
                                })
    
    return dataframe

@pytest.fixture(scope='function')
def entity_fixture():
    
    dataframe = pandas.DataFrame({
                                  'Site Address':['','1234 Smith St No Name, CO','','',''],
                                  'Owner1':['','SOURIVONGS KAM LLC','','',''],
                                  'Skip Trace1':['','SOURIVONGS KAM LLC','','','']
                                })

    return dataframe
    

@pytest.fixture(scope='function')
def fixture():
    """
    Stub dataframe & dlc and mock WebDriver
    """
    

    dataframe = pandas.DataFrame({
                                  'Site Address':['','1234 Smith St No Name, CO','','',''],
                                  'Owner1':['','SMITH JOHN','','',''],
                                  'Owner2':['','SMITH JANE','','',''],
                                  'Skip Trace1':['','SMITH JOHN','','',''],
                                  'Skip Trace2':['','SMITH JANE','','','']
                                })

    dlc = {
             'dataframe': dataframe,
             'first_name': 'JOHN',
             'mid_initial': '',
             'last_name': 'SMITH',
             'address': '1234 Smith St No Name, CO',
             'city': 'No Name',
             'state': 'CO',
             'data': 1,
             'top_priority_list': [],
             'high_priority_list': [],
             'skip_found': False,
             '100+': False,
             'search_list': 'top',
             'cur_skip': 'JOHN SMITH',
             'name_skip': 'John Smith',
             'location_skip': 'No Name, CO',
             'status_counter': 1,
             'a_counter': 1,
             'deceased': False,
             'owners': ['SMITH JOHN','SMITH JANE']
          }

    driver = MagicMock()

    return dlc, driver


@pytest.fixture(scope='function')
def skip_found_fixture():
    """
    Stub dataframe & dlc and mock WebDriver
    """
    

    dataframe = pandas.DataFrame({
                                  'Site Address':['','1234 Smith St No Name, CO','','',''],
                                  'Owner1':['','SMITH JOHN','','',''],
                                  'Owner2':['','SMITH JANE','','',''],
                                  'Skip Trace1':['','SMITH JOHN','','',''],
                                  'Skip Trace2':['','SMITH JANE','','',''],
                                })

    dlc = {
             'dataframe': dataframe,
             'first_name': 'JOHN',
             'mid_initial': '',
             'last_name': 'SMITH',
             'address': '1234 Smith St No Name, CO',
             'city': 'No Name',
             'state': 'CO',
             'data': 1,
             'top_priority_list': [1],
             'high_priority_list': [],
             'skip_found': True,
             '100+': False,
             'search_list': 'top',
             'cur_skip': 'JOHN SMITH',
             'name_skip': 'John Smith',
             'location_skip': 'No Name, CO',
             'status_counter': 1,
             'a_counter': 1,
             'deceased': False,
             'owners': ['SMITH JOHN','SMITH JANE']
          }

    driver = MagicMock()

    return dlc, driver
