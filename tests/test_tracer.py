# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------Libraries/Imports-------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import os
import re
import pytest

import sys
sys.path.append(r'..\Skip Tracer')

import tracer

from unittest.mock import patch, MagicMock
from selenium.common.exceptions import TimeoutException

# ----------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------Test Cases/Test Suite----------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


def test_procure_skips(dataframe_fixture):
    """
    Ensure skips are secured from given dataframe
    """

    dataframe = dataframe_fixture
    
    owners = tracer.procure_skips(dataframe)

    assert owners == [
                      {'Address':'','Owner1':'','Owner2':''},
                      {'Address':'1234 Smith St No Name, CO',
                       'Owner1':'SMITH JOHN',
                       'Owner2':'SMITH JANE'},
                      {'Address':'','Owner1':'','Owner2':''},
                      {'Address':'','Owner1':'','Owner2':''},
                      {'Address':'','Owner1':'','Owner2':''}
                     ]

    
@pytest.mark.parametrize('route', [
    ('pass'),
    ('fail')
])
def test_solve_recaptcha(route):
    """
    Ensure proper recaptcha event routing
    """

    driver = MagicMock()    # Dummy driver

    with patch('tracer.time'), \
         patch('pywinauto.mouse.click'), \
         patch('tracer.download_complete', return_value = True), \
         patch('os.listdir', return_value = '[\'audio.mp3\']'), \
         patch('subprocess.call'), \
         patch('os.remove'), \
         patch('speech_recognition.Recognizer'), \
         patch('speech_recognition.AudioFile'), \
         patch('pywinauto.keyboard.SendKeys'), \
         patch('tracer.WebDriverWait') as mock_WebDriverWait:

        if route == 'pass':
            pass

        else:
            mock_WebDriverWait.side_effect = TimeoutException  
        
        pass_or_fail, driver = tracer.solve_recaptcha(driver)
              
        assert pass_or_fail == route
        

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass
