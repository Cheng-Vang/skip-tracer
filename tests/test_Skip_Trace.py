# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------Libraries/Imports-------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import os
import re
import pytest

import sys
sys.path.append(r'..\Skip Tracer')

import skip
import tracer
import Skip_Tracer

from datetime import datetime
from unittest.mock import patch, MagicMock

# ----------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------Test Cases/Test Suite----------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


@pytest.mark.parametrize('sitrep', [
    ('Iterate and untraceable'),
    ('Incompatible city parameter'),
    ('Entity')
])
def test_skip_trace(dataframe_fixture, entity_fixture, fixture, sitrep):
    """
    Ensure proper routing of skip tracing operations
    """

    if sitrep == 'Iterate and untraceable':
        sitrep_alpha(dataframe_fixture, fixture)

    elif sitrep == 'Incompatible city parameter':
        sitrep_bravo(dataframe_fixture)

    else:
        sitrep_charlie(entity_fixture, fixture)

        
def sitrep_alpha(dataframe_fixture, fixture):
    """
    Operations for iteration and untraceable
    """
    
    dataframe = dataframe_fixture
    dlc, driver = fixture
    path = 'Path 99-99-9999.xlsx'
    owners = [
              {},
              {'Owner1': 'SMITH JOHN',
               'Address': '1234 Smith St No Name, CO',
               'Owner2': 'SMITH JANE'},
              {},
              {},
              {}
              ]

    with patch('tracer.select', return_value = [dataframe, path]), \
         patch('tracer.procure_skips', return_value = owners), \
         patch('skip.login'), \
         patch('skip.initiate_skip_trace', return_value = [dlc, driver]), \
         patch('tracer.log'),\
         patch('pandas.DataFrame.to_excel'):

        dlc = Skip_Tracer.skip_trace()

    assert dlc['dataframe'].loc[dlc['data'], 'Skip Trace1 Status'] == 'Untraceable'
    assert dlc['dataframe'].loc[dlc['data'], 'Skip Trace2 Status'] == 'Untraceable'
    assert dlc['status_counter'] == 3
    

def sitrep_bravo(dataframe_fixture):
    """
    Operations for incompatible city parameter
    """
    
    dataframe = dataframe_fixture
    path = 'Path 99-99-9999.xlsx'
    owners = [
              {},
              {'Owner1': 'SMITH JOHN',
               'Address': '1234 Smith St Incompatible City, CO'},
              {},
              {},
              {}
              ]

    with patch('tracer.select', return_value = [dataframe, path]), \
         patch('tracer.procure_skips', return_value = owners), \
         patch('skip.login'), \
         patch('tracer.log'),\
         patch('pandas.DataFrame.to_excel'):

        dlc = Skip_Tracer.skip_trace()

    assert dlc['dataframe'].loc[1, 'Skip Trace1 Status'] == 'Incompatible City Parameter'
    assert dlc['status_counter'] == 2


def sitrep_charlie(entity_fixture, fixture):
    """
    Operations for entity
    """
    
    dataframe = entity_fixture
    dlc, driver = fixture
    path = 'Path 99-99-9999.xlsx'
    owners = [
              {},
              {'Owner1': 'SMITH JOHN LLC',
               'Address': '1234 Smith St No Name, CO'},
              {},
              {},
              {}
              ]

    with patch('tracer.select', return_value = [dataframe, path]), \
         patch('tracer.procure_skips', return_value = owners), \
         patch('skip.login'), \
         patch('skip.initiate_skip_trace', return_value = [dlc, driver]), \
         patch('tracer.log'), \
         patch('pandas.DataFrame.to_excel'):

        dlc = Skip_Tracer.skip_trace()

    assert dlc['dataframe'].loc[1, 'Skip Trace1 Status'] == 'Entity'
    assert dlc['status_counter'] == 1


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass
